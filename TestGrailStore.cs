﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Text;

namespace LMP.Grail
{
    public class TestGrailStore: Grail.GrailBackend
    {
        OleDbConnection m_oCn;
        string[,] m_aMappings;

        public TestGrailStore()
        {
            //populate array of variable/field mappings
            string[] aSourceFieldNames = this.GetSourceDataFieldNames();
            string[] aMacPacFieldNames = this.GetMacPacVariableNames();

            m_aMappings = new string[aSourceFieldNames.Length, 2];

            for (int i = 0; i < aSourceFieldNames.Length; i++)
            {
                m_aMappings[i, 0] = aSourceFieldNames[i];
                m_aMappings[i, 1] = aMacPacFieldNames[i];
            }
        }
        #region GrailBackend Members

        public override void Connect(string[] aConnectionData)
        {

            //get path from registry
            string xDataDir = Registry.GetLocalMachineValue(
                LMP.Registry.MacPac10RegistryRoot, "DataDirectory");

            if (System.String.IsNullOrEmpty(xDataDir))
            {
                //no registry path was specified - get from root directory
                xDataDir = Registry.GetLocalMachineValue(
                    LMP.Registry.MacPac10RegistryRoot, "RootDirectory") + @"\data";
            }

            xDataDir = EvaluateEnvironmentVariables(xDataDir);

            m_oCn = new OleDbConnection(
                @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                xDataDir +
                @"\TestGrailStore.accdb" + ";Jet OLEDB:System database=" +
                xDataDir + @"\Forte.mdw;User ID=MacPacPersonnel;Password=fish4mill");

            //connect to admin db
            m_oCn.Open();
        }

        public override bool IsConnected
        {
            get
            {
                if (m_oCn == null)
                    return false;
                else
                    return (m_oCn.State & System.Data.ConnectionState.Open) == System.Data.ConnectionState.Open;
            }
        }

        public override int MaxRecordsRetrievable
        {
            get { return 5000; }
        }

        public override Grail.Clients GetClients(string xClientNameFilter, string xAdditionalFilter, Grail.SortField iSortBy)
        {
            string xSQL = "SELECT ID, ClientName FROM Clients ";

            if (!string.IsNullOrEmpty(xClientNameFilter) || !string.IsNullOrEmpty(xAdditionalFilter))
                xSQL += "WHERE ";

            if (!string.IsNullOrEmpty(xClientNameFilter))
            {
                if (IsInt(xClientNameFilter))
                    xSQL += "ID=" + xClientNameFilter + " ";
                else
                    xSQL += "ClientName LIKE '%" + xClientNameFilter + "%' ";
            }

            if (!string.IsNullOrEmpty(xAdditionalFilter))
            {
                xSQL += xAdditionalFilter;
            }

            if (iSortBy == SortField.Name)
            {
                xSQL += " ORDER BY ClientName";
            }

            OleDbCommand oCmd = new OleDbCommand(xSQL, m_oCn);
            OleDbDataReader oReader = oCmd.ExecuteReader();
            
            Clients oClients = new Clients();

            //move to starting row
            for (int i = 1; oReader.Read(); i++)
            {
                if (i > (this).MaxRecordsRetrievable)
                    return oClients;

                //get values at current row
                object[] oValues = new object[oReader.FieldCount];
                oReader.GetValues(oValues);

                //create new client object and add to collection
                oClients.Add(oValues[1].ToString(), oValues[0].ToString(), null, this);
            }

            return oClients;
        }

        public override Grail.Clients GetClients(string xClientNameFilter, Grail.SortField iSortBy)
        {
            return (this).GetClients(xClientNameFilter, null, iSortBy);
        }

        public override Grail.Clients GetClients(Grail.SortField iSortBy)
        {
            return (this).GetClients(null, null, iSortBy);
        }

        public override Grail.Matters GetMatters(Grail.Client oClient, string xMatterFilter, string xAdditionalFilter, Grail.SortField iSortBy)
        {
            string xSQL = "SELECT ID, ClientID, Description FROM Matters ";

            if (oClient != null || !string.IsNullOrEmpty(xMatterFilter) || !string.IsNullOrEmpty(xAdditionalFilter))
                xSQL += "WHERE ";

            if (oClient != null)
            {
                xSQL += "ClientID=" + oClient.Number + " ";
            }

            if (!string.IsNullOrEmpty(xMatterFilter))
            {
                if (xSQL.Contains("WHERE ClientID="))
                    xSQL += " AND ";

                if (IsInt(xMatterFilter))
                    xSQL += "ID=" + xMatterFilter + " ";
                else
                    xSQL += "Description LIKE '%" + xMatterFilter + "%' ";
            }

            if (!string.IsNullOrEmpty(xAdditionalFilter))
            {
                if (xSQL.Contains("WHERE ClientID=") || xSQL.Contains("WHERE ID=") || xSQL.Contains("WHERE Description LIKE"))
                    xSQL += " AND ";

                xSQL += xAdditionalFilter;
            }

            if (iSortBy == SortField.Name)
            {
                xSQL += " ORDER BY ClientID,Description;";
            }
            else
            {
                xSQL += "ORDER BY ClientID;";
            }

            OleDbCommand oCmd = new OleDbCommand(xSQL, m_oCn);
            OleDbDataReader oReader = oCmd.ExecuteReader();
            Matters oMatters = new Matters(oClient);

            //move to starting row
            for (int i = 1; oReader.Read(); i++)
            {
                if (i >= (this).MaxRecordsRetrievable)
                {
                    return oMatters;
                }

                //get values at current row
                object[] oValues = new object[oReader.FieldCount];
                oReader.GetValues(oValues);

                //create new client object and add to collection
               oMatters.Add(oValues[2].ToString(), oValues[1].ToString() + "." + oValues[0].ToString(),
                   oClient.Number);
            }

            return oMatters;
        }

        public override Grail.Matters GetMatters(Grail.Client oClient, string xMatterFilter, Grail.SortField iSortBy)
        {
            return (this).GetMatters(oClient, xMatterFilter, null, iSortBy);
        }

        public override Grail.Matters GetMatters(Grail.Client oClient, Grail.SortField iSortBy)
        {
            return (this).GetMatters(oClient, null, null, iSortBy);
        }

        public override bool IsValidClientMatterNumber(string xClientID, string xMatterID)
        {
            throw new NotImplementedException();
        }

        public string[] GetMacPacVariableNames()
        {
            return new string[2] { "Test1", "Test2" };
        }

        public string[] GetSourceDataFieldNames()
        {
            return new string[2] { "ClientAddress", "Judge" };
        }

        public override string GetData(string xID, params string[] xMacPacVariableName)
        {
            throw new NotImplementedException();
        }

        public override string GetData(int iID, params string[] xMacPacVariableName)
        {
            throw new NotImplementedException();
        }

        public override string GetData(string xID)
        {
            int iPos = xID.IndexOf(".");
            string xClientNumber = xID.Substring(0, iPos);
            string xMatterNumber = xID.Substring(iPos + 1);

            //query for client matter detail
            string xSQL = "SELECT m.ID as MatterNumber, c.ID as ClientNumber, '" + xClientNumber.PadLeft(4,'0') + "-" + xMatterNumber.PadLeft(4,'0') + 
                "' as ClientMatterNumber, * FROM Matters m INNER JOIN Clients c ON m.ClientID=c.ID WHERE m.ID=" + 
                xMatterNumber + " AND m.ClientID=" + xClientNumber;

            OleDbCommand oCmd = new OleDbCommand(xSQL, m_oCn);
            OleDbDataReader oReader = oCmd.ExecuteReader(System.Data.CommandBehavior.SingleRow);

            if (oReader.Read())
            {
                //get field names
                DataTable oDT = oReader.GetSchemaTable();

                //get values
                object[] aValues = new object[oReader.FieldCount];
                oReader.GetValues(aValues);

                StringBuilder oSB = new StringBuilder();

                //cycle through values, creating field/value pair string
                for (int i = 0; i < aValues.Length; i++)
                {
                    string xColName = oDT.Rows[i][0].ToString();

                    if (aValues[i].ToString().Contains("Þ"))
                    {
                        //field contains multiple data items - append
                        oSB.Append(aValues[i].ToString());
                    }
                    else
                    {
                        //field is simple
                        oSB.AppendFormat("{0}¤{1}Þ", xColName, aValues[i].ToString());
                    }
                }

                //add starting delimiter
                string xPrefillString = "Þ" + oSB.ToString();

                for (int i = 0; i < m_aMappings.GetLength(0); i++)
                {
                    xPrefillString = xPrefillString.Replace(
                        "Þ" + m_aMappings[i, 0] + "¤", "Þ" + m_aMappings[i, 1] + "¤");
                }

                return xPrefillString;
            }
            else
            {
                return null;
            }
        }

        public override string GetData(int iID)
        {
            throw new NotImplementedException();
        }

        public override int ClientsCount(string xClientNameFilter, string xAdditionalFilter)
        {
            string xSQL = "SELECT COUNT(ID) FROM Clients ";

            if (!string.IsNullOrEmpty(xClientNameFilter) || !string.IsNullOrEmpty(xAdditionalFilter))
                xSQL += "WHERE ";

            if (!string.IsNullOrEmpty(xClientNameFilter))
            {
                if (IsInt(xClientNameFilter))
                    xSQL += "ID=" + xClientNameFilter + " ";
                else
                    xSQL += "ClientName LIKE '%" + xClientNameFilter + "%' ";
            }

            if (!string.IsNullOrEmpty(xAdditionalFilter))
            {
                xSQL += xAdditionalFilter;
            }

            OleDbCommand oCmd = new OleDbCommand(xSQL, m_oCn);
            object oCount = oCmd.ExecuteScalar();

            return (int)oCount;
        }

        public override int MattersCount(Grail.Client oClient, string xMatterFilter, string xAdditionalFilter)
        {
            string xSQL = "SELECT ID, ClientID, Description FROM Matters ";

            if (oClient != null || !string.IsNullOrEmpty(xMatterFilter) || !string.IsNullOrEmpty(xAdditionalFilter))
                xSQL += "WHERE ";

            if (oClient != null)
            {
                xSQL += "ClientID=" + oClient.Number + " ";
            }

            if (!string.IsNullOrEmpty(xMatterFilter))
            {
                if (xSQL.Contains("WHERE ClientID="))
                    xSQL += " AND ";

                if (IsInt(xMatterFilter))
                    xSQL += "ID=" + xMatterFilter + " ";
                else
                    xSQL += "Description LIKE '%" + xMatterFilter + "%' ";
            }

            if (!string.IsNullOrEmpty(xAdditionalFilter))
            {
                if (xSQL.Contains("WHERE ClientID=") || xSQL.Contains("WHERE ID=") || xSQL.Contains("WHERE Description LIKE"))
                    xSQL += " AND ";

                xSQL += xAdditionalFilter;
            }

            OleDbCommand oCmd = new OleDbCommand(xSQL, m_oCn);
            object oCount = oCmd.ExecuteScalar();

            return (int)oCount;
        }

        public override Matters GetMatters(string xMatterFilter, SortField iSortBy)
        {
            string xSQL = "SELECT ID, ClientID, Description FROM Matters ";

            if (!string.IsNullOrEmpty(xMatterFilter))
            {
                xSQL += "WHERE ";

                if (IsInt(xMatterFilter))
                    xSQL += "ID=" + xMatterFilter + " ";
                else
                    xSQL += "Description LIKE '%" + xMatterFilter + "%' ";
            }

            if (iSortBy == SortField.Name)
            {
                xSQL += " ORDER BY ClientID,Description;";
            }
            else
            {
                xSQL += "ORDER BY ClientID;";
            }

            OleDbCommand oCmd = new OleDbCommand(xSQL, m_oCn);
            OleDbDataReader oReader = oCmd.ExecuteReader();
            Matters oMatters = new Matters(this);

            //move to starting row
            for (int i = 1; oReader.Read(); i++)
            {
                if (i > (this).MaxRecordsRetrievable)
                {
                    return oMatters;
                }

                //get values at current row
                object[] oValues = new object[oReader.FieldCount];
                oReader.GetValues(oValues);

                //create new client object and add to collection
                oMatters.Add(oValues[2].ToString(), oValues[1].ToString() + "." + oValues[0].ToString(), 
                    oValues[1].ToString());
            }

            return oMatters;
        }

        #endregion

        public override void Disconnect()
        {
            m_oCn.Close();
        }

        public override void SaveFile(Microsoft.Office.Interop.Word.Document oDoc, string xClientMatterNumber, string xDocName, string xDocType)
        {
            throw new NotImplementedException();
        }

        public override GrailBackend.mpGrailClientMatterDisplayFormat DisplayFormat
        {
            get { return mpGrailClientMatterDisplayFormat.ClientMatter; }
        }

        private bool IsInt(string text)
        {
            int num = 0;

            try
            {
                num = int.Parse(text);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private static string EvaluateEnvironmentVariables(string xText)
        {

            //evaluate path for environment variables
            int iPos = xText.IndexOf('%');
            while (iPos > -1)
            {
                int iPos2 = xText.IndexOf('%', iPos + 1);

                if (iPos2 == -1)
                {
                    //something's wrong
                    throw new System.Exception("Invalid environment variable format");
                }

                string xEnvVar = xText.Substring(iPos + 1, iPos2 - iPos - 1);

                //evaluate
                xEnvVar = System.Environment.GetEnvironmentVariable(xEnvVar);

                xText = xText.Substring(0, iPos) + xEnvVar + xText.Substring(iPos2 + 1);

                iPos = xText.IndexOf('%', iPos + 1);
            }

            return xText;
        }

    }
}
